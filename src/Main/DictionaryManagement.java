package Main;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class DictionaryManagement {
    private Dictionary dictionary = new Dictionary();
    private static final String PATH = "src\\dictionaries.txt";

    public Dictionary getDictionary() {
        return dictionary;
    }

    public static String nextString(String str) {
        char x = (char) ((int) str.charAt(str.length() - 1) + 1);
        return (str.substring(0, str.length() - 1) + x);
    }

    /**
     * read number from commandline.
     *
     * @return int
     */
    public static int readNumberFromCommandline() {
        Scanner sc = new Scanner(System.in);
        String str = "";
        while (str.equals("")) {
            str = sc.next();
            str = str.trim();
        }
        if (str.equals("")) return (-1);
        for (int i = 0; i < str.length(); i++) {
            if ((int) str.charAt(i) < (int) '0' || (int) str.charAt(i) > (int) '9') return (-1);
        }
        return Integer.valueOf(str);
    }

    /**
     * read String from commandline.
     *
     * @return String
     */
    public static String readStringFromCommandline() {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        str = str.trim();
        while (str.length() == 0) {
            str = sc.nextLine();
            str = str.trim();
        }
        return str;
    }

    /**
     * insert from commandline.
     */
    public void insertFromCommandline() {

        System.out.print("Nhập số lượng từ mới: ");
        int wordsNumber = readNumberFromCommandline();
        if (wordsNumber == -1) {
            System.out.println("Số lượng từ không hợp lệ!");
            return;
        }
        for (int i = 1; i <= wordsNumber; i++) {
            System.out.print("Nhập từ mới " + i + ": ");
            String wordTarget = readStringFromCommandline();
            if (this.dictionary.existed(wordTarget)) {
                System.out.println("Từ mới đã tồn tại!");
                continue;
            }
            System.out.print("Nhập nghĩa từ mới " + i + ": ");
            String wordExplain = readStringFromCommandline();
            Word newWord = new Word(wordTarget, wordExplain);
            this.dictionary.insert(newWord);
            System.out.println("Đã thêm thành công!");
        }
    }

    /**
     * remove from commandline.
     */
    public void removeFromCommandline() {
        System.out.print("Nhập số lượng từ cần xóa: ");
        int wordsNumber = readNumberFromCommandline();
        if (wordsNumber == -1) {
            System.out.println("Số lượng từ không hợp lệ!");
            return;
        }
        for (int i = 1; i <= wordsNumber; i++) {
            System.out.print("Nhập từ cần xóa: ");
            String wordTarget = readStringFromCommandline();
            if (this.dictionary.existed(wordTarget)) {
                this.dictionary.remove(wordTarget);
                System.out.println("Đã xóa thành công!");
            } else {
                System.out.println("Từ cần xóa không tồn tại!");
            }
        }
    }

    /**
     * search from commandline.
     */
    public void searchFromCommanline() {
        System.out.print("Nhập từ cần tra cứu: ");
        String beginWordTarget = readStringFromCommandline();
        SortedSet<Word> resultWords = this.dictionary.prefixSearch(beginWordTarget);
        int i = 0;
        for (Word word : resultWords) {
            word.show();
        }
    }

    /**
     * insert words from file.
     *
     * @throws IOException
     */
    public void insertFromFile() throws IOException {
        BufferedReader buffReader = new BufferedReader(
                new InputStreamReader(new FileInputStream(PATH), StandardCharsets.UTF_8));
        String str;
        while ((str = buffReader.readLine()) != null) {
            String[] words = str.split("\t");
            if (words.length == 2) {
                this.dictionary.insert(new Word(words[0], words[1]));
            }
        }
        buffReader.close();
    }

    /**
     * export dictionary to file.
     *
     * @throws IOException
     */
    public void exportToFile() throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(PATH), StandardCharsets.UTF_8));
        TreeSet<Word> allWords = this.dictionary.getAllWords();
        for (Word word : allWords) {
            String wordTarget = word.getWord_target();
            String wordExplain = word.getWord_explain();
            bufferedWriter.write(wordTarget + '\t');
            bufferedWriter.write(wordExplain.replace("\n", "#"));
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
    }

}
