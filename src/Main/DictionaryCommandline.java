package Main;

import java.io.IOException;
import java.util.Scanner;
import java.util.TreeSet;

public class DictionaryCommandline {
    private DictionaryManagement dictionaryManagement = new DictionaryManagement();

    /**
     * show all words from dictionary.
     */
    public void showAllWords() {
        TreeSet<Word> allWords = dictionaryManagement.getDictionary().getAllWords();
        int index = 0;
        for (Word word : allWords) {
            index++;
            String s1 = String.valueOf(index);
            System.out.print(s1 + ". ");
            word.show();
        }
    }

    /**
     * Setup.
     *
     * @throws IOException
     */
    public void setUp() throws IOException {
        this.dictionaryManagement.insertFromFile();
    }

    /**
     * add words to dictionary.
     */
    public void addWords() {
        this.dictionaryManagement.insertFromCommandline();
    }

    /**
     * remove words from dictionary.
     */
    public void removeWords() {
        this.dictionaryManagement.removeFromCommandline();
    }

    /**
     * search word.
     */
    public void searchWord() {
        this.dictionaryManagement.searchFromCommanline();
    }

    /**
     * close and export new file.
     *
     * @throws IOException
     */
    public void close() throws IOException {
        this.dictionaryManagement.exportToFile();
    }

    /**
     * main void for dictionary commandline.
     *
     * @param args String[]
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        DictionaryCommandline myApp = new DictionaryCommandline();
        myApp.setUp();
        while (true) {
            System.out.println("Gõ phím [1] để hiện thị tất cả các từ.");
            System.out.println("Gõ phím [2] để thêm các từ mới.");
            System.out.println("Gõ phím [3] để xóa các từ.");
            System.out.println("Gõ phím [4] để tra cúu từ.");
            System.out.println("Gõ phím [0] để lưu và thoát chương trình.");
            String input1 = DictionaryManagement.readStringFromCommandline();
            switch (input1) {
                case "1":
                    myApp.showAllWords();
                    break;
                case "2":
                    myApp.addWords();
                    break;
                case "3":
                    myApp.removeWords();
                    break;
                case "4":
                    myApp.searchWord();;
                    break;
                case "0": {
                    myApp.close();
                    return;
                }
                default:
                    System.out.println("Nhập sai yêu cầu!");
            }
        }
    }
}
