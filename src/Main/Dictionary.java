package Main;

import java.util.SortedSet;
import java.util.TreeSet;

public class Dictionary {
    private TreeSet<Word> allWords = new TreeSet<Word>();

    public TreeSet<Word> getAllWords() {
        return allWords;
    }

    public boolean existed(String wordTarget) {
        return this.allWords.contains(new Word(wordTarget, ""));
    }

    public void insert(Word newWord) {
        this.allWords.add(newWord);
    }

    public void remove(String wordTarget) {
        this.allWords.remove(new Word(wordTarget, ""));
    }

    /**
     * search with prefix String
     *
     * @param prefix String
     * @return Sortedset <Word></Word>
     */
    public SortedSet<Word> prefixSearch(String prefix) {
        char x = (char) ((int) prefix.charAt(prefix.length() - 1) + 1);
        return (getSubSet(prefix, prefix.substring(0, prefix.length() - 1) + x));
    }

    public SortedSet<Word> getSubSet(String beginWordTarget, String endWordTarget) {
        return this.allWords.subSet(new Word(beginWordTarget, ""), new Word(endWordTarget, ""));
    }

    public Word getWord(String wordTarget) {
        return (prefixSearch(wordTarget).first());
    }

}
