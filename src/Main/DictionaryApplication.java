package Main;

import GUI.AddWordGUI.AddWordGUI;
import GUI.DictionaryGUI.DictionaryGUI;
import GUI.Menu;
import GUI.translateGUI.TranslatorGUI;

import javax.swing.*;
import java.io.IOException;

public class DictionaryApplication {
    private DictionaryManagement dictionaryManagement = new DictionaryManagement();
    private JFrame mainFrame = new JFrame("1112's Dictionary");
    private JPanel mainPanel = new JPanel();
    private int frameWidth = 750;
    private int frameHeight = 603;
    private String selectedPanel;

    public DictionaryApplication() throws IOException {
    }

    public DictionaryManagement getDictionaryManagement() {
        return dictionaryManagement;
    }

    public void setDictionaryManagement(DictionaryManagement dictionaryManagement) {
        this.dictionaryManagement = dictionaryManagement;
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }

    public void setMainFrame(JFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public void setMainPanel(JPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    public int getFrameWidth() {
        return frameWidth;
    }

    public void setFrameWidth(int frameWidth) {
        this.frameWidth = frameWidth;
    }

    public int getFrameHeight() {
        return frameHeight;
    }

    public void setFrameHeight(int frameHeight) {
        this.frameHeight = frameHeight;
    }

    public void setUp() throws IOException {
        this.dictionaryManagement.insertFromFile();
    }

    public void run() throws IOException {
        mainFrame.setSize(frameWidth, frameHeight);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setResizable(false);

        Menu menu = new Menu();
        menu.addListener(this);
        mainFrame.setJMenuBar(menu.getMenuBar());

        DictionaryGUI dictionaryGUI = new DictionaryGUI(this);
        mainPanel = dictionaryGUI.getDictionaryPanel();
        mainFrame.add(mainPanel);
        mainFrame.setVisible(true);
        selectedPanel = "dictionary";
    }

    public void loadDictionaryGUI() throws IOException {
        if (selectedPanel.equals("dictionary")) return;
        mainFrame.remove(mainPanel);
        DictionaryGUI dictionaryGUI = new DictionaryGUI(this);
        mainPanel = dictionaryGUI.getDictionaryPanel();
        mainFrame.add(mainPanel);
        mainFrame.setVisible(true);
        selectedPanel = "dictionary";
    }

    public void loadTranslatorGUI() throws IOException {
        if (selectedPanel.equals("translator")) return;
        mainFrame.remove(mainPanel);
        TranslatorGUI translatorGUI = new TranslatorGUI(this);
        mainPanel = translatorGUI.getTranslatorPanel();
        mainFrame.add(mainPanel);
        mainFrame.setVisible(true);
        selectedPanel = "translator";
    }

    public void loadAddInput() throws IOException {
        mainFrame.remove(mainPanel);
        AddWordGUI addWordGUI = new AddWordGUI(this);
        mainPanel = addWordGUI.getPanel();
        mainFrame.add(mainPanel);
        mainFrame.setVisible(true);
        selectedPanel = "addInput";
    }

    public static void main(String[] args) throws IOException {
        DictionaryApplication myApp = new DictionaryApplication();
        myApp.setUp();
        myApp.run();
    }
}
