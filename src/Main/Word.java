package Main;

public class Word implements Comparable<Word> {
    private String word_target;
    private String word_explain;

    public String getWord_target() {
        return word_target;
    }

    public void setWord_target(String word_target) {
        this.word_target = word_target;
    }

    public String getWord_explain() {
        return word_explain;
    }

    public String getShowWord() {
        return word_explain.replace("#", "\n");
    }

    public void setWord_explain(String word_explain) {
        this.word_explain = word_explain;
    }

    public Word(String word_target, String word_explain) {
        this.word_target = word_target;
        this.word_explain = word_explain;
    }

    /**
     * show words in commandline.
     */
    public void show() {
        System.out.println(this.getWord_target());
        System.out.println(this.getShowWord());
    }

    @Override
    public int compareTo(Word anotherWord) {
        return (this.word_target.compareToIgnoreCase(anotherWord.getWord_target()));
    }

}
