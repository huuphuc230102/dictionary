package GoogleAPI;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.json.JSONObject;

public class GoogleSpellCheck {
    /**
     * fix wrong search input of dictionary to correct.
     *
     * @param text String
     * @return String
     * @throws Exception https://serpapi.com/spell-check
     */
    public String fixSpell(String text) throws Exception {
        String apikey = "37c9c152fc80cdf4d606ff563111426799077f320d6293bd53c4a42ad20b71f3";
        String api = "https://serpapi.com/search.json?q="
                + URLEncoder.encode(text, String.valueOf(StandardCharsets.UTF_8))
                + "&hl=en&gl=us&api_key=" + apikey;
        URL url = new URL(api);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        String res = convertFixResult(content.toString());
        if (res.equals("")) return new String(text);
        return res;
    }

    /**
     * format jsonString from json file and return spelling-fix text.
     *
     * @param jsonString String
     * @return String
     */
    private String convertFixResult(String jsonString) {
        String ans = "";
        JSONObject obj = new JSONObject(jsonString);
        JSONObject obj2 = (JSONObject) obj.get("search_information");
        if (obj2.has("spelling_fix"))
            return (String) obj2.get("spelling_fix");
        return "";
    }

}
