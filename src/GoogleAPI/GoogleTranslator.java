package GoogleAPI;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class GoogleTranslator {

    /**
     * get translate result of google translate from input text.
     *
     * @param sourceLanguage String language of input text
     * @param targetLanguage String language of target
     * @param text           String
     *                       https://docs.oracle.com/javase/tutorial/networking/urls/readingWriting.html
     *                       https://stackoverflow.com/questions/8147284/how-to-use-google-translate-api-in-my-java-application
     */
    public String translateResult(String sourceLanguage, String targetLanguage, String text) throws Exception {
        text = text.trim();
        if (text.length() == 0) {
            return "";
        }
        String api = "https://translate.google.com/translate_a/single?client=gtx&sl="
                + sourceLanguage + "&tl=" + targetLanguage + "&hl=" + targetLanguage
                + "&dt=at&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=sos&dt=ss&dt=t&otf=2&ssel=0&tsel=0&q="
                + URLEncoder.encode(text, "UTF-8");
        URL url = new URL(api);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        return convertTranslation(content.toString());
    }

    /**
     * format jsonString from json file to normal String.
     *
     * @param jsonString String
     * @return String
     */
    private String convertTranslation(String jsonString) {
        String ans = "";
        JSONArray arr = new JSONArray(jsonString);
        // meaning
        JSONArray temp = new JSONArray(arr.get(0).toString());
        JSONArray meaningArray = new JSONArray(temp.get(0).toString());
        ans += (String) ("\n" + meaningArray.get(0) + "\n");
        if (arr.get(1).toString().equals("null")) return ans;
        // details meaning
        JSONArray subarr = new JSONArray(arr.get(1).toString());
        for (Object obj : subarr) {
            JSONArray example = new JSONArray(obj.toString());
            ans += (String) ("\n-  " + example.get(0) + ":  ");
            JSONArray exampleWords = new JSONArray(example.get(1).toString());
            for (Object o2 : exampleWords) {
                ans += (String) (o2.toString() + ", ");
            }
            ans = ans.substring(0, ans.length() - 2);
            ans += (String) ("\n");
        }
        return ans;
    }


    /**
     * main function to test Google translator.
     *
     * @param args args.
     * @throws Exception exception.
     */
    public static void main(String[] args) throws Exception{
        GoogleTranslator A = new GoogleTranslator();
        String result = A.translateResult("en", "vi", "a dog");
        System.out.println(result);

    }
}