package GoogleAPI;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class GooglePronounce {
    /**
     * get translate result of google translate from input text.
     * @param language String language of word need to speak
     * @param text String
     * https://c5zone.forumotion.com/t7528-thu-thuat-phat-nhac-mp3-trong-java
     */
    public void pronounce(String language, String text) throws IOException, JavaLayerException {
        String api = "http://translate.google.com/translate_tts?ie=UTF-8&tl=" + language + "&client=tw-ob&q=" +
                URLEncoder.encode(text, String.valueOf(StandardCharsets.UTF_8));
        URL url = new URL(api);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        //con.setRequestProperty("User-Agent", "Mozilla/5.0");
        InputStream audio = con.getInputStream();
        new Player(audio).play();
        con.disconnect();
    }

    /**
     * main void test google pronounce
     */
    public static void main(String[] args) throws IOException, JavaLayerException {
        GooglePronounce A = new GooglePronounce();
        A.pronounce("en", "");
    }
}
