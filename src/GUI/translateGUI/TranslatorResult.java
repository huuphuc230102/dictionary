package GUI.translateGUI;

import GoogleAPI.GoogleTranslator;

import javax.swing.*;
import java.awt.*;

public class TranslatorResult {
    private JTextArea area;
    private JScrollPane scrollPane;

    public JScrollPane getScrollPane() {
        return scrollPane;
    }

    public void setScrollPane(JScrollPane scrollPane) {
        this.scrollPane = scrollPane;
    }

    public JTextArea getArea() {
        return area;
    }

    public void setArea(JTextArea area) {
        this.area = area;
    }

    /**
     * constructor of Translator Result with text input
     *
     * @param text String
     */
    public TranslatorResult(String text) {
        this.area = new JTextArea();
        this.area.setEditable(false);
        this.area.setFont(new Font("Calibri", Font.PLAIN, 18));
        this.area.setLineWrap(true);
        this.area.setBackground(new Color(240, 240, 240));
        GoogleTranslator ggTranslate = new GoogleTranslator();
        String result = "";
        try {
            result += ggTranslate.translateResult("en", "vi", text);
            result = result.trim();
        } catch (Exception err) {
            err.printStackTrace();
            result = "No Internet Connection !!!";
        }
        this.area.setText(result);
        this.area.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));

        this.scrollPane = new JScrollPane(this.area);
        this.scrollPane.setBounds(380, 195, 320, 300);
        this.scrollPane.setBorder(BorderFactory.createLineBorder(Color.darkGray, 2));
    }
}
