package GUI.translateGUI;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class TranslatorSearch {
    private JTextArea area;
    private JScrollPane scrollPane;

    public JTextArea getarea() {
        return area;
    }

    public void setarea(JTextArea area) {
        this.area = area;
    }

    public JScrollPane getScrollPane() {
        return scrollPane;
    }

    public void setScrollPane(JScrollPane scrollPane) {
        this.scrollPane = scrollPane;
    }

    /**
     * Constructor of translation search.
     */
    public TranslatorSearch() {
        this.area = new JTextArea();
        this.area.setFont(new Font("Calibri", Font.PLAIN, 18));
        this.area.setLineWrap(true);
        this.area.setBackground(new Color(240, 240, 240));
        this.area.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));
        this.scrollPane = new JScrollPane(this.area);
        this.scrollPane.setBounds(30, 195, 320, 300);
        this.scrollPane.setBorder(BorderFactory.createLineBorder(Color.darkGray, 2));
    }

    /**
     * update when search new text
     *
     * @param translatorGUI TranslatorGUI
     */
    public void updateSearchString(TranslatorGUI translatorGUI) {
        String text = this.area.getText().trim();
        translatorGUI.Search(text);
    }

    /**
     * add listeners of input area
     *
     * @param translatorGUI TranslatorGUI
     */
    public void addListener(TranslatorGUI translatorGUI) {
        area.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSearchString(translatorGUI);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSearchString(translatorGUI);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateSearchString(translatorGUI);
            }
        });
    }
}