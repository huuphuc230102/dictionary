package GUI.translateGUI;

import Main.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class TranslatorGUI {
    private DictionaryApplication app;
    private JPanel translatorPanel;
    private JLabel logoImage;
    private TranslatorSearch translatorSearch;
    private TranslatorResult translatorResult;

    public JPanel getTranslatorPanel() {
        return translatorPanel;
    }

    public JLabel getLogoImage() throws IOException {
        BufferedImage myPicture = ImageIO.read(new File("src\\public\\logo.jpg"));
        JLabel picLabel = new JLabel(new ImageIcon(myPicture));
        return picLabel;
    }

    /**
     * constructor 1 param of translator GUI
     *
     * @param app DictionaryApplication
     * @throws IOException
     */
    public TranslatorGUI(DictionaryApplication app) throws IOException {
        this.app = app;
        translatorPanel = new JPanel();
        translatorPanel.setLayout(null);
        translatorPanel.setBackground(new Color(47, 79, 78));
        translatorPanel.setBounds(0, 0, app.getFrameWidth(), app.getFrameHeight());

        logoImage = getLogoImage();
        logoImage.setBounds(0, -10, app.getFrameWidth(), 129);

        JLabel label1 = new JLabel("Văn bản tiếng Anh: ");
        JLabel label2 = new JLabel("Bản dịch tiếng Việt: ");
        Font font = new Font("Calibri", Font.PLAIN, 25);
        label1.setFont(font);
        label2.setFont(font);
        label1.setForeground(Color.WHITE);
        label2.setForeground(Color.WHITE);
        label1.setBounds(35, 145, 300, 50);
        label2.setBounds(395, 145, 300, 50);

        translatorPanel.add(logoImage);
        translatorPanel.add(label1);
        translatorPanel.add(label2);

        this.translatorSearch = new TranslatorSearch();
        this.translatorPanel.add(translatorSearch.getScrollPane());
        this.translatorSearch.addListener(this);

        this.translatorResult = new TranslatorResult("");
        translatorPanel.add(translatorResult.getScrollPane());
    }

    public void load() {
        app.setMainPanel(translatorPanel);
    }

    /**
     * change result when search new text.
     *
     * @param text String
     */
    public void Search(String text) {
        translatorPanel.remove(translatorResult.getScrollPane());
        translatorResult = new TranslatorResult(text);
        translatorPanel.add(translatorResult.getScrollPane());
        translatorPanel.revalidate();
        translatorPanel.repaint();
    }

}
