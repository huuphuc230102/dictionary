package GUI.DictionaryGUI;

import Main.DictionaryApplication;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class AddButton {
    private JButton button;
    private int buttonWidth = 47;
    private int buttonHeight = 47;

    public int getButtonWidth() {
        return buttonWidth;
    }

    public void setButtonWidth(int buttonWidth) {
        this.buttonWidth = buttonWidth;
    }

    public int getButtonHeight() {
        return buttonHeight;
    }

    public void setButtonHeight(int buttonHeight) {
        this.buttonHeight = buttonHeight;
    }

    public JButton getButton() {
        return button;
    }

    public void setButton(JButton button) {
        this.button = button;
    }

    public ImageIcon getIcon() throws IOException {
        BufferedImage img = ImageIO.read(new File("src\\public\\add.jpg"));
        Image dimg = img.getScaledInstance(buttonWidth, buttonHeight, Image.SCALE_SMOOTH);
        return new ImageIcon(dimg);
    }

    public AddButton() throws IOException {
        this.button = new JButton(getIcon());
        this.button.setBounds(535, 142, buttonWidth, buttonHeight);
    }

    /**
     * add listener of add button in dictionary app.
     *
     * @param app DictionaryApplication
     */
    public void addListener(DictionaryApplication app) {
        button.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    app.loadAddInput();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

}
