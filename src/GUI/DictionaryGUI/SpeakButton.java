package GUI.DictionaryGUI;

import GoogleAPI.GooglePronounce;
import Main.Dictionary;
import Main.Word;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class SpeakButton {
    private JButton button;
    private int buttonWidth = 33;
    private int buttonHeight = 33;

    public int getButtonWidth() {
        return buttonWidth;
    }

    public void setButtonWidth(int buttonWidth) {
        this.buttonWidth = buttonWidth;
    }

    public int getButtonHeight() {
        return buttonHeight;
    }

    public void setButtonHeight(int buttonHeight) {
        this.buttonHeight = buttonHeight;
    }

    public JButton getButton() {
        return button;
    }

    public void setDeleteButton(JButton button) {
        this.button = button;
    }

    public ImageIcon getIcon() throws IOException {
        BufferedImage img = ImageIO.read(new File("src\\public\\speak.jpg"));
        Image dimg = img.getScaledInstance(buttonWidth, buttonHeight, Image.SCALE_SMOOTH);
        return new ImageIcon(dimg);
    }

    public SpeakButton() throws IOException {
        this.button = new JButton(getIcon());
        this.button.setBounds(593, 155, buttonWidth, buttonHeight);
    }

    /**
     * add listener of speak button in dictionary GUI.
     *
     * @param dictionaryGUI dictionaryGUI
     */
    public void addListener(DictionaryGUI dictionaryGUI) {
        this.button.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String mean = dictionaryGUI.getExplainArea().getArea().getText();
                String[] text = mean.split("\n");
                GooglePronounce speaker = new GooglePronounce();
                if (text[0].length() > 0)
                    try {
                        speaker.pronounce("en", text[0]);
                    } catch (Exception err) {
                        err.printStackTrace();
                    }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

}
