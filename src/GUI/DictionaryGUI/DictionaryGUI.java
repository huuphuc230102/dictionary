package GUI.DictionaryGUI;

import GoogleAPI.GoogleSpellCheck;
import Main.Dictionary;
import Main.DictionaryApplication;
import Main.Word;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class DictionaryGUI {
    private DictionaryApplication app;
    private JPanel dictionaryPanel;
    private JLabel logoImage;
    private AddButton addButton;
    private DeleteButton deleteButton;
    private SpeakButton speakButton;
    private SearchInput searchInput;
    private SearchResult searchResult;
    private ExplainArea explainArea;
    private SaveButton saveButton;

    public SearchResult getSearchResult() {
        return searchResult;
    }

    public GUI.DictionaryGUI.ExplainArea getExplainArea() {
        return explainArea;
    }

    public JPanel getDictionaryPanel() {
        return dictionaryPanel;
    }

    public DictionaryApplication getApp() {
        return app;
    }

    public JLabel getLogoImage() throws IOException {
        BufferedImage myPicture = ImageIO.read(new File("src\\public\\logo.jpg"));
        JLabel picLabel = new JLabel(new ImageIcon(myPicture));
        return picLabel;
    }

    /**
     * Constructor of DictionaryGui.
     *
     * @param app DictionaryApplication
     */
    public DictionaryGUI(DictionaryApplication app) throws IOException {
        this.app = app;
        dictionaryPanel = new JPanel();
        dictionaryPanel.setLayout(null);
        dictionaryPanel.setBackground(new Color(47, 79, 78));
        dictionaryPanel.setBounds(0, 0, app.getFrameWidth(), app.getFrameHeight());

        logoImage = getLogoImage();
        logoImage.setBounds(0, -10, app.getFrameWidth(), 129);
        dictionaryPanel.add(logoImage);

        addButton = new AddButton();
        addButton.addListener(app);
        dictionaryPanel.add(addButton.getButton());

        deleteButton = new DeleteButton();
        dictionaryPanel.add(deleteButton.getButton());
        deleteButton.addListener(this);

        speakButton = new SpeakButton();
        speakButton.addListener(this);
        dictionaryPanel.add(speakButton.getButton());

        searchInput = new SearchInput();
        dictionaryPanel.add(searchInput.getTextField());
        dictionaryPanel.add(searchInput.getFixLabel());
        searchInput.addListener(this);

        saveButton = new SaveButton();
        dictionaryPanel.add(saveButton.getButton());
        saveButton.addListener(this);

        searchResult = new SearchResult(app, searchInput.getTextField().getText());
        searchResult.addListener(this);
        dictionaryPanel.add(searchResult.getScrollPane());
        dictionaryPanel.add(searchResult.getNumberResults());

        explainArea = new ExplainArea("");
        dictionaryPanel.add(explainArea.getScrollPane());
    }

    public void load() {
        app.setMainPanel(dictionaryPanel);
    }

    /**
     * update SearchResult.
     *
     * @param prefixString String
     */
    public void updateSearchResult(String prefixString) {
        dictionaryPanel.remove(searchResult.getScrollPane());
        dictionaryPanel.remove(searchResult.getNumberResults());
        searchResult = new SearchResult(this.app, prefixString);
        searchResult.addListener(this);
        dictionaryPanel.add(searchResult.getScrollPane());
        dictionaryPanel.add(searchResult.getNumberResults());
        dictionaryPanel.revalidate();
        dictionaryPanel.repaint();

        explainArea.getArea().setText("");
    }

    /**
     * update ExplainArea.
     *
     * @param word Word
     */
    public void updateExplainArea(Word word) {
        String text = word.getWord_target() + '\n';
        text += word.getShowWord();
        dictionaryPanel.remove(explainArea.getScrollPane());
        explainArea = new ExplainArea(text);
        dictionaryPanel.add(explainArea.getScrollPane());
        dictionaryPanel.revalidate();
        dictionaryPanel.repaint();
    }

    /**
     * active delete button.
     *
     * @throws IOException
     */
    public void activeDeleteButton() throws IOException {
        JList list = searchResult.getList();
        if (list.getSelectedValue() == null) return;

        String wordTarget = (String) list.getSelectedValue();
        String message = " Bạn có muốn xóa từ '" + wordTarget + "' khỏi từ điển không ? ";

        int result = JOptionPane.showConfirmDialog(app.getMainFrame(), message, "Xác nhận",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (result == JOptionPane.YES_OPTION) {

            Dictionary dictionary = app.getDictionaryManagement().getDictionary();
            dictionary.remove(wordTarget);
            updateSearchResult(searchInput.getTextField().getText());

            JOptionPane.showMessageDialog(app.getMainFrame(),
                    "                    Đã xóa thành công!", "Thông báo", JOptionPane.PLAIN_MESSAGE);

        }
    }

    /**
     * active save button.
     *
     * @throws IOException
     */
    public void activeSaveButton() throws IOException {
        app.getDictionaryManagement().exportToFile();
        JOptionPane.showMessageDialog(app.getMainFrame(),
                "                    Đã lưu thành công!", "Thông báo", JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * fix input when wrong.
     */
    public void fixInput() {
        System.out.println(1);
        String text = searchInput.getTextField().getText().trim();
        GoogleSpellCheck spellFixer = new GoogleSpellCheck();
        try {
            String newInput = spellFixer.fixSpell(text);
            if (app.getDictionaryManagement().getDictionary().existed(newInput)) {
                searchInput.getTextField().setText(newInput);
                updateSearchResult(newInput);
            }
        } catch (Exception err) {
            err.printStackTrace();
        }

    }

}
