package GUI.DictionaryGUI;

import javax.swing.*;
import java.awt.*;

public class ExplainArea {
    private JTextArea area;
    private JScrollPane scrollPane;

    public JScrollPane getScrollPane() {
        return scrollPane;
    }

    public void setScrollPane(JScrollPane scrollPane) {
        this.scrollPane = scrollPane;
    }

    public JTextArea getArea() {
        return area;
    }

    public void setArea(JTextArea area) {
        this.area = area;
    }

    public ExplainArea(String text) {
        this.area = new JTextArea();
        this.area.setEditable(false);
        this.area.setFont(new Font("Calibri", Font.PLAIN, 18));
        this.area.setLineWrap(true);
        this.area.setText(text);
        this.area.setBackground(new Color(240, 240, 240));
        this.area.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));

        this.scrollPane = new JScrollPane(this.area);
        this.scrollPane.setBounds(292, 200, 422, 330);
        this.scrollPane.setBorder(BorderFactory.createLineBorder(Color.darkGray, 2));
    }
}
