package GUI.DictionaryGUI;

import Main.Dictionary;
import Main.DictionaryApplication;
import Main.Word;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.SortedSet;

public class SearchResult {
    private JList list;
    private JScrollPane scrollPane;
    private JLabel numberResults;

    public JLabel getNumberResults() {
        return numberResults;
    }

    public JList getList() {
        return list;
    }

    public void setList(JList list) {
        this.list = list;
    }

    public JScrollPane getScrollPane() {
        return scrollPane;
    }

    public void setScrollPane(JScrollPane scrollPane) {
        this.scrollPane = scrollPane;
    }

    public SearchResult(DictionaryApplication app, String prefixString) {
        prefixString = prefixString.trim();
        DefaultListModel<String> listModel = new DefaultListModel<>();
        Dictionary dictionary = app.getDictionaryManagement().getDictionary();
        if (prefixString.equals("")) {
            for (Word word : dictionary.getAllWords()) {
                listModel.addElement(word.getWord_target());
            }
        } else {
            SortedSet<Word> prefixSearchWords = dictionary.prefixSearch(prefixString);
            for (Word word : prefixSearchWords) {
                listModel.addElement(word.getWord_target());
            }
        }

        this.list = new JList(listModel);
        this.list.setFont(new Font("Calibri", Font.PLAIN, 22));

        this.list.setBackground(new Color(240, 240, 240));

        this.scrollPane = new JScrollPane(this.list);
        this.scrollPane.setBounds(18, 198, 260, 330);
        this.scrollPane.setBorder(BorderFactory.createLineBorder(Color.darkGray, 2));

        numberResults = new JLabel("");
        numberResults.setFont(new Font("Calibri", Font.ITALIC, 15));
        numberResults.setForeground(Color.lightGray);
        numberResults.setBounds(30, 173, 100, 30);
        numberResults.setText(String.valueOf(list.getModel().getSize()) + " kết quả");
    }

    /**
     * add listeners select word from result scroll.
     *
     * @param dictionaryGUI Dictionary GUI
     */
    public void addListener(DictionaryGUI dictionaryGUI) {
        this.list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                Dictionary dictionary = dictionaryGUI.getApp().getDictionaryManagement().getDictionary();
                Word word = dictionary.getWord((String) list.getSelectedValue());
                dictionaryGUI.updateExplainArea(word);
            }
        });
    }


}
