package GUI.DictionaryGUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class DeleteButton {
    private JButton button;
    private int buttonWidth = 33;
    private int buttonHeight = 33;

    public int getButtonWidth() {
        return buttonWidth;
    }

    public void setButtonWidth(int buttonWidth) {
        this.buttonWidth = buttonWidth;
    }

    public int getButtonHeight() {
        return buttonHeight;
    }

    public void setButtonHeight(int buttonHeight) {
        this.buttonHeight = buttonHeight;
    }

    public JButton getButton() {
        return button;
    }

    public void setButton(JButton button) {
        this.button = button;
    }

    public ImageIcon getIcon() throws IOException {
        BufferedImage img = ImageIO.read(new File("src\\public\\delete.jpg"));
        Image dimg = img.getScaledInstance(buttonWidth, buttonHeight, Image.SCALE_SMOOTH);
        return new ImageIcon(dimg);
    }

    /**
     * constructor of deletebutton.
     *
     * @throws IOException
     */
    public DeleteButton() throws IOException {
        this.button = new JButton(getIcon());
        this.button.setBounds(636, 155, buttonWidth, buttonHeight);
    }

    /**
     * add listener of delete button in dictionary GUI.
     *
     * @param dictionaryGUI Dictionary Gui
     */
    public void addListener(DictionaryGUI dictionaryGUI) {
        button.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    dictionaryGUI.activeDeleteButton();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

}
