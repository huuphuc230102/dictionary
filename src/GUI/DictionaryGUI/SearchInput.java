package GUI.DictionaryGUI;

import GoogleAPI.GoogleSpellCheck;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class SearchInput {
    private JTextField textField;
    private GoogleSpellCheck spellChecker;
    private JLabel fixLabel;

    public JLabel getFixLabel() {
        return fixLabel;
    }

    public JTextField getTextField() {
        return textField;
    }

    public void setTextField(JTextField textField) {
        this.textField = textField;
    }

    /**
     * constructor no param of search input.
     */
    public SearchInput() {
        this.textField = new JTextField();
        this.textField.setBounds(18, 132, 260, 40);
        Font font = new Font("Calibri", Font.PLAIN, 24);
        this.textField.setFont(font);
        this.textField.setBackground(new Color(240, 240, 240));
        this.textField.setBorder(BorderFactory.createLineBorder(Color.darkGray, 2));

        fixLabel = new JLabel("");
        fixLabel.setFont(new Font("Calibri", Font.BOLD, 18));
        fixLabel.setForeground(Color.green);
        fixLabel.setBounds(170, 172, 200, 30);
    }

    /**
     * update when change input string in dictionary GUI.
     *
     * @param dictionaryGUI DictionaryGUI.
     */
    public void updateInputString(DictionaryGUI dictionaryGUI) {
        String text = this.textField.getText().trim();
        dictionaryGUI.updateSearchResult(text);
        if (dictionaryGUI.getSearchResult().getList().getModel().getSize() == 0) {
            fixLabel.setText("autocorrect");
            dictionaryGUI.updateSearchResult(text);
        } else {
            fixLabel.setText("");
            dictionaryGUI.updateSearchResult(text);
        }
    }

    /**
     * add listener of text field and fix label in dictionary GUI.
     *
     * @param dictionaryGUI DictionaryGUI
     */
    public void addListener(DictionaryGUI dictionaryGUI) {
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateInputString(dictionaryGUI);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateInputString(dictionaryGUI);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateInputString(dictionaryGUI);
            }
        });
        fixLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //fixLabel.setText("");

                dictionaryGUI.fixInput();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                fixLabel.setForeground(Color.BLUE);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                fixLabel.setForeground(Color.green);
            }

        });

    }
}
