package GUI;

import Main.DictionaryApplication;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class Menu {
    private JMenuBar menuBar;
    private JMenu guiMenu;
    public JMenuItem dictionaryMenuItem;
    public JMenuItem translatorMenuItem;

    public JMenuBar getMenuBar() {
        return menuBar;
    }

    public void setMenuBar(JMenuBar menuBar) {
        this.menuBar = menuBar;
    }

    public JMenu getGuiMenu() {
        return guiMenu;
    }

    public void setGuiMenu(JMenu guiMenu) {
        this.guiMenu = guiMenu;
    }

    public JMenuItem getDictionaryMenuItem() {
        return dictionaryMenuItem;
    }

    public void setDictionaryMenuItem(JMenuItem dictionaryMenuItem) {
        this.dictionaryMenuItem = dictionaryMenuItem;
    }

    public JMenuItem getTranslatorMenuItem() {
        return translatorMenuItem;
    }

    public void setTranslatorMenuItem(JMenuItem translatorMenuItem) {
        this.translatorMenuItem = translatorMenuItem;
    }

    public Menu() {
        menuBar = new JMenuBar();
        guiMenu = new JMenu("Menu");
        dictionaryMenuItem = new JMenuItem("Từ điển");
        translatorMenuItem = new JMenuItem("Dịch");

        guiMenu.add(dictionaryMenuItem);
        guiMenu.add(translatorMenuItem);

        dictionaryMenuItem.setBackground(Color.LIGHT_GRAY);
        menuBar.add(guiMenu);
    }

    public void addListener(DictionaryApplication app) {
        dictionaryMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dictionaryMenuItem.setBackground(Color.LIGHT_GRAY);
                translatorMenuItem.setBackground(null);
                try {
                    app.loadDictionaryGUI();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        });

        translatorMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dictionaryMenuItem.setBackground(null);
                translatorMenuItem.setBackground(Color.LIGHT_GRAY);
                try {
                    app.loadTranslatorGUI();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
