package GUI.AddWordGUI;

import Main.Dictionary;
import Main.DictionaryApplication;
import Main.Word;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class AddWordGUI {
    private DictionaryApplication app;
    private JPanel panel;
    private int width = 500;
    private int height = 400;
    private JLabel logoImage;
    private WordTargetField wordTargetField;
    private WordExplainArea wordExplainArea;
    private Buttons buttons;

    public DictionaryApplication getApp() {
        return app;
    }

    public void setApp(DictionaryApplication app) {
        this.app = app;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JLabel getLogoImage() throws IOException {
        BufferedImage myPicture = ImageIO.read(new File("src\\public\\logo.jpg"));
        JLabel picLabel = new JLabel(new ImageIcon(myPicture));
        return picLabel;
    }

    /**
     * constructor of AddWordGui
     *
     * @param app DictionaryApplication
     * @throws IOException
     */
    public AddWordGUI(DictionaryApplication app) throws IOException {
        this.app = app;
        panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(new Color(47, 79, 78));
        panel.setBounds(100, 100, width, height);
        // panel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));

        logoImage = getLogoImage();
        logoImage.setBounds(0, -10, app.getFrameWidth(), 129);

        JLabel label1 = new JLabel("Nhập từ: ");
        JLabel label2 = new JLabel("Nhập nghĩa: ");
        Font font = new Font("Calibri", Font.PLAIN, 25);
        label1.setFont(font);
        label2.setFont(font);
        label1.setForeground(Color.WHITE);
        label2.setForeground(Color.WHITE);
        label1.setBounds(130, 130, 200, 50);
        label2.setBounds(130, 220, 200, 50);

        panel.add(logoImage);
        panel.add(label1);
        panel.add(label2);

        wordTargetField = new WordTargetField();
        panel.add(wordTargetField.getTextField());

        wordExplainArea = new WordExplainArea();
        panel.add(wordExplainArea.getScrollPane());

        buttons = new Buttons();
        buttons.addListener(this);
        panel.add(buttons.getAcceptButton());
        panel.add(buttons.getCancelButton());

    }

    /**
     * active accept button.
     */
    public void activeAcceptButton() {
        String wordTarget = wordTargetField.getTextField().getText();
        String wordExplain = wordExplainArea.getArea().getText();
        wordTarget = wordTarget.trim();
        wordExplain = wordExplain.trim();

        Dictionary dictionary = app.getDictionaryManagement().getDictionary();
        if (dictionary.existed(wordTarget)) {
            JOptionPane.showMessageDialog(app.getMainFrame(),
                    "       Từ này đã tồn tại!", "Lỗi", JOptionPane.ERROR_MESSAGE);
            wordTargetField.getTextField().setText("");
            wordExplainArea.getArea().setText("");
            return;
        }
        if (wordTarget.equals("")) {
            JOptionPane.showMessageDialog(app.getMainFrame(),
                    "     Bạn chưa nhập từ mới!", "Lỗi", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (wordExplain.equals("")) {
            JOptionPane.showMessageDialog(app.getMainFrame(),
                    " Bạn chưa nhập nghĩa của từ!", "Lỗi", JOptionPane.ERROR_MESSAGE);
            return;
        }

        dictionary.insert(new Word(wordTarget, wordExplain));
        dictionary.insert(new Word(wordTarget, wordExplain));
        JOptionPane.showMessageDialog(app.getMainFrame(),
                "                   Đã thêm thành công!", "Thông báo", JOptionPane.PLAIN_MESSAGE);
        wordTargetField.getTextField().setText("");
        wordExplainArea.getArea().setText("");

    }

    /**
     * active cancel button.
     *
     * @throws IOException
     */
    public void activeCancelButton() throws IOException {
        app.loadDictionaryGUI();
    }
}
