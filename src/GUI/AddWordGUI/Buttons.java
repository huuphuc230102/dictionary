package GUI.AddWordGUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

public class Buttons {
    private JButton acceptButton;
    private JButton cancelButton;

    public JButton getAcceptButton() {
        return acceptButton;
    }

    public void setAcceptButton(JButton acceptButton) {
        this.acceptButton = acceptButton;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public void setCancelButton(JButton cancelButton) {
        this.cancelButton = cancelButton;
    }

    /**
     * constructor no param of buttons in addword GUI.
     */
    public Buttons() {
        Font font = new Font("Calibri", Font.PLAIN, 21);
        acceptButton = new JButton("Thêm từ");
        acceptButton.setFont(font);
        acceptButton.setBounds(180, 475, 150, 36);

        cancelButton = new JButton("Hủy");
        cancelButton.setFont(font);
        cancelButton.setBounds(400, 475, 150, 36);

    }

    /**
     * add listeners of buttons in addword GUI.
     *
     * @param addWordGUI
     */
    public void addListener(AddWordGUI addWordGUI) {
        acceptButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                addWordGUI.activeAcceptButton();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        cancelButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    addWordGUI.activeCancelButton();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }
}
