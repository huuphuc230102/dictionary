package GUI.AddWordGUI;

import GUI.DictionaryGUI.DictionaryGUI;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

public class WordTargetField {
    private JTextField textField;

    public JTextField getTextField() {
        return textField;
    }

    public void setTextField(JTextField textField) {
        this.textField = textField;
    }

    /**
     * constructor no param of target field in addword Gui.
     */
    public WordTargetField() {
        this.textField = new JTextField();
        this.textField.setBounds(130, 175, 470, 40);
        this.textField.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        Font font = new Font("Calibri", Font.PLAIN, 22);
        this.textField.setFont(font);
        this.textField.setBackground(new Color(240, 240, 240));
        this.textField.setBorder(BorderFactory.createLineBorder(Color.darkGray, 2));
    }

}
