package GUI.AddWordGUI;

import javax.swing.*;
import java.awt.*;

public class WordExplainArea {
    private JTextArea area;
    private JScrollPane scrollPane;

    public JScrollPane getScrollPane() {
        return scrollPane;
    }

    public void setScrollPane(JScrollPane scrollPane) {
        this.scrollPane = scrollPane;
    }

    public JTextArea getArea() {
        return area;
    }

    public void setArea(JTextArea area) {
        this.area = area;
    }

    /**
     * constructor of explain area in add word gui.
     */
    public WordExplainArea() {
        this.area = new JTextArea();
        this.area.setFont(new Font("Calibri", Font.PLAIN, 22));
        this.area.setLineWrap(true);
        this.area.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        this.scrollPane = new JScrollPane(this.area);
        this.scrollPane.setBounds(130, 265, 470, 190);
        this.area.setBackground(new Color(240, 240, 240));
        this.scrollPane.setBorder(BorderFactory.createLineBorder(Color.darkGray, 2));
    }
}
